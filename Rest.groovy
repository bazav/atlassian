import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.transform.BaseScript

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

@BaseScript CustomEndpointDelegate delegate

searchLot { MultivaluedMap queryParams ->
   
    def dialog =
        """<section role="dialog" id="sr-dialog-small" class="aui-layer aui-dialog2 aui-dialog2-small" aria-hidden="true" data-aui-remove-on-hide="true">
            <header class="aui-dialog2-header">             
                <h2 class="aui-dialog2-header-main">В поле пишем номер лота</h2>
                
                <div class="aui-dialog2-header-secondary">
            		<form class="aui" action="#">
                		<input id="valueField" class="text" type="text" name="search">
            		</form>
        		</div>
                <a class="aui-dialog2-header-close">
                    <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                </a>
            </header>
            
            <div class="aui-dialog2-content">
                <p>Тут будет поиск по лоту... </p>
            </div>
            
            <footer class="aui-dialog2-footer">
            
                <div class="aui-dialog2-footer-actions">
            		<button  id="buttonSearch_small" class="aui-button aui-button-primary" >Поиск</button>
                    <button id="dialog-close-button2" class="aui-button aui-button-link">Close</button>
        		</div>
       
                <div class="aui-dialog2-footer-hint">По заказу Арсения</div>
                
            </footer>
        </section>
        
     #################################################   
     
        <section role="dialog" id="sr-dialog-xlarge" class="aui-layer aui-dialog2 aui-dialog2-xlarge" aria-hidden="true" data-aui-remove-on-hide="true">
            <header class="aui-dialog2-header">             
                <h2 class="aui-dialog2-header-main">Результаты полиска</h2>
                <!--
                <div class="aui-dialog2-header-secondary">
            		<form class="aui" action="#">
                		<input id="valueField" class="text" type="text" name="search">
            		</form>
        		</div>
                <a class="aui-dialog2-header-close">
                    <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                </a> -->
            </header>
            
            <div class="aui-dialog2-content">
                <table class="aui">
                    <thead>
                        <tr>
                            <th id="name">Name</th>
                            <th id="type">Type</th>
                            <th id="order">Order</th>
                            <th id="action">Action</th>
                        </tr>
                    </thead>
                </table>    
            </div>
            
            <footer class="aui-dialog2-footer">
            
                <div class="aui-dialog2-footer-actions">
            		<button  id="buttonSearch_xlarge" class="aui-button aui-button-primary" >Новый поиск</button>
                    <button id="dialog-close-button" class="aui-button aui-button-link">Close</button>
        		</div>
       
                <div class="aui-dialog2-footer-hint">По заказу Арсения</div>
                
            </footer>
        </section>
        
    ###########################    
    
        <script>
            	\$("#sr-dialog-small #buttonSearch_small").on('click', function(e) {
    				e.preventDefault();
   	 				AJS.dialog2("#sr-dialog-small").hide();
                    AJS.dialog2("#sr-dialog-xlarge").show();
				});
                
                \$("#sr-dialog-xlarge #buttonSearch_xlarge").on('click', function(e) {
    				e.preventDefault();
   	 				AJS.dialog2("#sr-dialog-small").show();
                    AJS.dialog2("#sr-dialog-xlarge").hide();
				});
        </script>
        
        
        """

    Response.ok().type(MediaType.TEXT_HTML).entity(dialog.toString()).build()
}