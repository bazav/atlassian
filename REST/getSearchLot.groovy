import com.atlassian.jira.component.ComponentAccessor
//https://coderoad.ru/36698657/%D0%9A%D0%B0%D0%BA-%D0%BF%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C-%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F-%D0%BF%D0%BE-email-%D0%B2-JIRA-Script-Runner
import javax.servlet.http.HttpServletRequest
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.transform.BaseScript
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.query.Query
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response


import org.apache.log4j.Level
import org.apache.log4j.Logger
 
log = Logger.getLogger("com.acme.CreateFile")
log.setLevel(Level.DEBUG)


@BaseScript CustomEndpointDelegate delegate

getSearchLot(httpMethod: "GET") { MultivaluedMap queryParams, String body, HttpServletRequest request ->
 
def lot = request.getParameter("lot") 

def  customFieldManager = ComponentAccessor.getCustomFieldManager()  
def cf = customFieldManager.getCustomFieldObjectByName("Эфирная категория")
//def cfVal = issue.getCustomFieldValue(cf)
JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
SearchService searchService = ComponentAccessor.getComponent(SearchService)
IssueManager issueManager = ComponentAccessor.getIssueManager()
ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
Query query = jqlQueryParser.parseQuery("'Лот I' ~ ${lot}")  
SearchResults search = searchService.search(user, query, PagerFilter.getUnlimitedFilter())
List<Issue> issues = search.getResults()

ArrayList<Map<String,String>> json = new ArrayList<Map<String,String>>()

def issueKey
def summary
issues.each{
	issueKey = issueManager.getIssueObject(it.key)
	summary = issueKey.getSummary()
    def qq = issueKey.getCustomFieldValue(cf)[0]
    json.add(issue:"$issueKey", summary:"$summary",qq:"$qq")
}    

    return Response.ok(new JsonBuilder(json).toString()).build();

  
}
