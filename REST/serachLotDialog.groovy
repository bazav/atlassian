import com.atlassian.jira.component.ComponentAccessor
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.query.Query
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import groovy.transform.BaseScript
 
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response
 
@BaseScript CustomEndpointDelegate delegate

 
searchLot { MultivaluedMap queryParams ->
def issue
def summary  
JqlQueryParser jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
SearchService searchService = ComponentAccessor.getComponent(SearchService)
IssueManager issueManager = ComponentAccessor.getIssueManager()
ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

Query query = jqlQueryParser.parseQuery("'Лот I' ~ 004-277-339")
SearchResults search = searchService.search(user, query, PagerFilter.getUnlimitedFilter())
List<Issue> issues = search.getResults()

issues.each{
  	issue = issueManager.getIssueObject(it.key)
	summary = issue.getSummary()
}
    
    
    
    def dialog =
        """<section role="dialog" id="sr-dialog-small" class="aui-layer aui-dialog2 aui-dialog2-small" aria-hidden="true" data-aui-remove-on-hide="true">
            <header class="aui-dialog2-header">             
                <h2 class="aui-dialog2-header-main">В поле пишем номер лота</h2>
                
                
                <div class="aui-dialog2-header-secondary state1">
                    <form class="aui" action="#">
                        <input id="valueField" class="text" type="text" name="search">
                    </form>
                </div>
                <a class="aui-dialog2-header-close state1">
                    <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                </a>
            </header>
            
            <div class="aui-dialog2-content state1">
                <p>Тут будет поиск по лоту... </p>
            </div>
            
            <div class="aui-dialog2-content state2" style="display: none;">
                <table class="aui">
                    <thead>
                        <tr>
                            <th id="issueKey">Номер задачи</th>
                            <th id="summary">Тема</th>
                            <th id="category">Эфирная категория</th>
                            <th id="date">Дата выхода рубрики в эфир</th>
                            <th id="path">Путь к файлу</th>
                            <th id="link">Ссылка на ютуб</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
            				<!-- <td headers="issueKey" href="#">${issue}</td> -->
                            <td headers="issueKey" onclick="location.href='http://jira-test-app.sands.local/browse/${issue}'"><a href="http://jira-test-app.sands.local/browse/${issue}">${issue}</a></td>
                            <td headers="summary">${summary}</td>
                            

                        </tr>   
                    </tbody>
                </table>   
            </div>
            
            <footer class="aui-dialog2-footer">            
                <div class="aui-dialog2-footer-actions">
                    <button type="button" id="search-button" class="aui-button aui-button-primary state1" >Поиск</button>
                    <button type="button" id="new-search-button" class="aui-button aui-button-primary state2" style="display: none;">Новый поиск</button>
                    <button type="button" id="dialog-close-button2" class="aui-button aui-button-link">Close</button>
                </div>
       
                <div class="aui-dialog2-footer-hint">По заказу Тогулева Арсения</div>
                
            </footer>
            
            <script>
            	const dialog = AJS.\$("#sr-dialog-small");
            	const content = dialog.find(".aui-dialog2-content");
               
            	dialog.find("#search-button").on('click', function(e) {
                	dialog.addClass("aui-dialog2-xlarge");
                	dialog.removeClass("aui-dialog2-small"); 
                    dialog.find(".state1").hide();
                    dialog.find(".state2").show();
                    var search = document.getElementById('valueField').value;
                });
                
                dialog.find("#new-search-button").on('click', function(e) {
                	dialog.removeClass("aui-dialog2-xlarge");
                	dialog.addClass("aui-dialog2-small"); 
                    dialog.find(".state2").hide();
                    dialog.find(".state1").show();                    
                });
        	</script>
        </section>        
        """
    
        
    
    Response.ok().type(MediaType.TEXT_HTML).entity(dialog.toString()).build()
}
